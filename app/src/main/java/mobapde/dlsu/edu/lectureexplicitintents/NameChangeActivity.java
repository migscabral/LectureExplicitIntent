package mobapde.dlsu.edu.lectureexplicitintents;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class NameChangeActivity extends AppCompatActivity {

    public static final int REQUEST_CODE = 1;

    private PersonPresenter mPersonPresenter;
    private EditText etName;
    private Button btnDone;
    private Activity baseActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_name_change);

        mPersonPresenter = new PersonPresenter();
        baseActivity = this;

        etName = (EditText) findViewById(R.id.etName);
        btnDone = (Button) findViewById(R.id.btnDone);

        btnDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mPersonPresenter.onNameChangeDoneListener(baseActivity, etName.getText().toString());
            }
        });
    }
}
