package mobapde.dlsu.edu.lectureexplicitintents;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    public static final String EXTRA_NAME = "name";

    private PersonPresenter mPersonPresenter;
    private ImageButton btnName;
    private TextView tvName;
    private Activity baseActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnName = (ImageButton) findViewById(R.id.btnName);
        tvName = (TextView) findViewById(R.id.tvName);

        mPersonPresenter = new PersonPresenter();

        baseActivity = this;

        btnName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mPersonPresenter.startNameChangeActivity(baseActivity, getBaseContext(), tvName.getText().toString());
            }
        });
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mPersonPresenter.updateDetailsMainActivity(requestCode, resultCode, data, tvName);
    }
}
