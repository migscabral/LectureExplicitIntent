package mobapde.dlsu.edu.lectureexplicitintents;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.widget.TextView;
import android.support.design.widget.Snackbar;

import static android.app.Activity.RESULT_OK;

/**
 * Created by migscabral on 05/10/2017.
 */

public class PersonPresenter {

    public void onNameChangeDoneListener(Activity baseActivity, String name) {
        if(name.isEmpty()){
            Snackbar.make(baseActivity.findViewById(android.R.id.content), "Name cannot be empty.", Snackbar.LENGTH_SHORT).show();
        }else {
            Intent data = new Intent();
            data.putExtra(MainActivity.EXTRA_NAME, name);
            baseActivity.setResult(RESULT_OK, data);
            baseActivity.finish();
        }
    }

    public void startNameChangeActivity(Activity baseActivity, Context baseContext, String name) {
        Intent intent = new Intent(baseContext, NameChangeActivity.class);
        intent.putExtra(MainActivity.EXTRA_NAME, name);
        baseActivity.startActivityForResult(intent, NameChangeActivity.REQUEST_CODE);
    }

    public void updateDetailsMainActivity(int requestCode, int resultCode, Intent data, TextView tvName) {
        if(resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case NameChangeActivity.REQUEST_CODE:
                    tvName.setText(data.getStringExtra(MainActivity.EXTRA_NAME));
                    break;
            }
        }
    }
}
